# Documentation: Do the Needful [`0.1.0`](CHANGELOG.md#)

For any given project, there are **three** types of _published_ documentation that a project maintainer can and should write.

This project is a pointer to those types of documentation, a discussion of why and how they're important, and an example of how to implement them in your own projects.

It also includes some useful practices around documentation that you might find to be useful patterns.

# Contributing / Feedback

We'd love for you to help us make this resource even better!
If you notice something wrong, or think we can improve, please do one of the following:

+ [File an issue](https://gitlab.com/michaeltlombardi/needful-docs/issues/new)
+ [Check our soure docs](https://gitlab.com/michaeltlombardi/needful-docs)
+ [Hit us up on Twitter](https://twitter.com/barbariankb)
+ [Email Us](mailto:michael.t.lombardi@outlook.com)
