# What is Reference Documentation?

Reference documentation can be defined like this:

> Information about the exact usage and functionality of a project which explains _precisely_ how to work within the project and elaborates on the parameters, types, and **technical** interfaces of the project.

Reference documentation in PowerShell, for example, is the comment-based help for advanced functions.
For a REST API project, the reference documentation might be written in [swagger](http://petstore.swagger.io/).
