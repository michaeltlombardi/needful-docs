# Why Write Reference Documentation?

Writing reference documentation:

+ Is often required by best practice for the type of project you're working on
+ Is necessary for explaining how your project functions both for other developers and for users
+ Requires minimum effort
+ Missing reference docs is a kiss-of-death for people looking to use your project
