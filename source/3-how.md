# How to Write Reference Documentation

This largely depends on the type of project you're working on.

Typically, if possible, your reference documentation should be _automatically updated and published_ from your code, not maintained by hand.

That way lies madness.

For PowerShell, consider using [PlatyPS](https://github.com/PowerShell/platyPS)
