# Type One: Reference Documentation

Reference documentation is:

1. The _most common_ form of documentation.
  + Almost everyone does this, most people do it halfway decently.
  + This is a good thing!
2. The **bare minimum** required documentation for a project to be useful.
  + If I don't have reference docs, how can I troubleshoot your project reliably?
  + How can I help other people adopt it if I have to explain everything myself?
3. **_NOT sufficient_**
  + Reference docs leave a bunch of information we still need to cover.
