#!/usr/bin/env bash
which gitbook || npm install gitbook-cli -g
which book    || npm install gitbook-summary -g

cp README.md    ./docs/README.md
cp CHANGELOG.md ./docs/CHANGELOG.md
cp LICENSE.md   ./docs/LICENSE.md
cp book.json    ./docs/book.json

mkdir ./docs/1-reference && cp ./source/* ./docs/1-reference/
cd ./docs
book sm
gitbook install && gitbook build . ../public
