# What Is Concept Documentation / What Are Guides?

Guides are documentation which explains, in detail, one of the following:

+ **Why** a design decision was made
+ **How** an internal component works and why it was used
+ **When** and _when not_ to use particular features or components
+ **What** to do when implementing this project in the user's environment
+ **Which** patterns are best practices and which patterns should be avoided
