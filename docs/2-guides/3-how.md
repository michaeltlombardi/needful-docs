# How To Write Guides

1. Keep notes when you're doing initial development
2. Iterate, don't increment - doesn't have to be a white paper
3. If you find yourself explaining a design decision to more than one person, transcribe that.
4. If people keep complaining about some function, feature, or component, explain how it _should_ be used.
5. As soon as you discover a good pattern of use, write it down
6. Get someone to annoy you about "What? Why? How?" and record the information from that
