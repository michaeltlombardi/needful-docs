# Why Write Guides?

1. Because you want to lower the bar to contribution
2. Because you want people using your project to be successful
3. Because you made your design decisions for a reason
4. Because you know your project better than anyone else
5. Because you know that these documents help drive adoption
