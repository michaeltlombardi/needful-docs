# Type Two: Guides / Concept Documentation

Guides and Concept documentation are:

1. The _least common_ form of documentation.
  + Almost no one does this, most people doing it are just paying lip service.
  + This is a **bad** thing!
2. A **best practice** for including documentation _if you want good contributions for your project_
  + If I don't have concept docs, how can I contribute to the project?
  + How can I properly implement your project in production without these docs?
  + Hint: The answer to both questions above is the same: _Not  very well..._
3. Not as hard to write as you think!
