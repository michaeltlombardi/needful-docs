# What Is Narrative Documentation?

Narrative documentation (also called tutorials) are documentation items which

+ Communicate how and why to perform actions in the context of a project
+ Do so by using _the structure of a story_
  + This doesn't **need** to be a full on short story, but being mindful of those techniques will hel
+ Have a character for the readers to identify with
  + This can be the author or the reader, but can also be an actual fictional character.
+ Focus on the transfer of knowledge and best practices to the reader
  + These docs are best for teaching; you have concet and reference documentation for more detailed things
+ The most powerful and effective form of documentation
  + Humans are wired to think in narratives.
