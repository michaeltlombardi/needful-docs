# Why Write Narrative Documentation / Tutorials

+ Narratives are how the human brain attempts to process complex systems
+ Human memory is more reliable when engaged by narratives than by reference docs
+ Narratives provide initial and immediate success for users
+ Narratives can drive first time to delight and thus adoption
+ Narratives can help users and contributors synthesize difficult and dense topics covered by guides
