# Type Three: Tutorials / How-Tos / Narrative Documentation

Tutorials and Narrative documentation are:

1. _Most commonly_ written not by the developers or maintainers, but by the _users_.
  + You can't stop users from submitting narrative documentation (and you shouldn't!)
  + You should _definitely_ be covering the most common use cases! _Especially_ the ones you wrote the project to solve in the first place!
2. A **best practice** for including documentation _if you want people to adopt your project_
  + If I don't have tutorials, how can I quickly get up and running?
  + How can I _quickly_ grok use cases for your project without tutorials?
  + How can I get a positive emotional feedback from your project?
  + Hint: The answer to the questions above is the same: _Not  very well..._
3. Totally okay to co-author and ask community members to help you!
  + But definitely, definitely, definitely write some yourself.
  + _Seriously._
