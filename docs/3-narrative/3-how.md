# How to Write Narrative Documentation / Tutorials
You can improve your narrative documentation by:

1. Start from a pseudo story
  + Sarah wants to use project `foo` to solve problem `bar`
  + Sarah takes step `A` for reasons `X` and `Y`
  + Sarah runs into an error while trying step `B`
  + Sarah uses `neat component` of project `foo` to work around `B`
  + Sarah successfully solves problem `bar` with `foo`
2. Review the story, clarify the actions required to solve the domain problem
3. Address potential pitfalls the character might run into during the story
  + This helps readers to learn without getting burned
4. Ensure that the narrative makes sense - read it aloud
5. Emphasize best practices for solving the domain problem with the tool/project
6. Run it through [Hemingway](http://www.hemingwayapp.com/) - simple writing is much better for sharing technical information.
  + But don't worry when it yells at you for code blocks, it doesn't know better (Hemingway wasn't much of a coder).
7. Clearly and explicitly tie to a version of the project and date-stamped
