# CHANGELOG

If you're working on a project which is:

+ ever going to change
+ could be used in production
+ might be used by humans other than the maintainer/developers

You _need_ a changelog.

I recommend you follow the [KeepAChangelog format](http://keepachangelog.com) and follow [Semantic Versioning](https://semver.org).

Both links will explain more on the how and why, but simply:

Changelogs are for humans and they make using your project safer and easier.
Keep one.
