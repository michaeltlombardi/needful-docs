# README

Your readme is the entry point into your project.

A readme should include:

1. A good, short explanation of _what problem your project solves_.
  + You wrote it for a reason, so let people know up front!
2. Tell users how to install/get your project!
  + If this is hard, no one will adopt it in any meaningful numbers.
3. Give a very short, relatively simple example!
  + Make it a useful one!
4. Link to your code base and the issue tracker!
  + Some people will find your docs from not-the-repo - point them back to it!
  + Lower the bar of contribution for filing an issue!
5. Let people know how best to contact the maintainers!
  + Slack, email, phone, twitter, whatever.
