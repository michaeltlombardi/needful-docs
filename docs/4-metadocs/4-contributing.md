# CONTRIBUTING

If you want people to improve or help with your project, or if you want people from teams outside your own to be able to contribute effectively, you need a contributing document.

A contributing document:

+ lets people know that you want contributions in general
+ shows that you've thought about the developers who might contribute to your project
+ enumerates project-specific requirements so contributors don't need to refactor their PRs all the time
+ helps give people ideas of where/how to contribute
+ helps developers actually get into the project to improve your work

There are some awesome examples online:

+ [Puppet](https://github.com/puppetlabs/puppet/blob/master/CONTRIBUTING.md)
+ [Chef](https://docs.chef.io/community_contributions.html)
+ [Brandon Olin's PoshBot](https://github.com/poshbotio/PoshBot/blob/master/.github/CONTRIBUTING.md)

It's pretty easy to template Brandon's for PowerShell!
