# Meta-Documents

There are five types of metadocuments your project _should_ have:

1. README
2. CHANGELOG
3. LICENSE
4. CONTRIBUTING
5. Templates
