# Template Documents

For whichever repository server you're using, you should also specify some project-level templates.

For github, you can create a `.github` folder in your project and drop in markdown for your contributing guide, issue template, and PR template.

For GitLab, you can specify the templates as markdown in your project settings.

Definitely consider stealing from [Brandon Olin's](https://twitter.com/devblackops) [awesome templates on Github](https://github.com/poshbotio/PoshBot/tree/master/.github). I did!

These make it really easy to drop your issue writers / PR submitters into a pit of success.

Do it.
