# LICENSE

Your project needs a license.

Even if you don't think anyone will use it.
Even if you are only sharing information, not code.
Even if you don't care what people do with it.

A license:

+ gives people explicit information about what they can and can't do with your project
+ lets orgs use your project
+ is dead easy to add.

You can use some awesome resources, like [Choose A License](https://choosealicense.com/), to figure out what license to use.
If you're with an org, get legal advice.

I recommend you default to easy and open (MIT / CC0).
